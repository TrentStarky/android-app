package com.example.starky.androidapp;

public class Ball
{
    private double width;
    private double height;
    private double speed;
    private double angle; //directly up = 0 degrees, rotating clockwise

    public Ball(double speed, double angle, int xPos, int yPos)
    {
        this.speed = speed;
        this.angle = angle;
        //set position to xPos and yPos
        //start ball moving in direction with speed
    }

    public void onPress()
    {
        //set score +1
        //create new ball with random angle, position and speed (in a certain range)
        //delete this object
    }

    public void outOfBounds(int edge) //int containing edge value, 1 = top, 2 = right, 3 = bottom, 4 = left
    {

    }
}